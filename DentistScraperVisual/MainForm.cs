﻿using CefSharp;
using DentistScraperVisual;
using Scraper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DentistScraperVisual
{
    public partial class MainForm : Form
    {
        private ScrapHelper scraper;
        private ScrapForm scrapForm;
        private Thread scrapThread;

        static ParseOptions dentistsParseOptions = new ParseOptions
        {
            BlockPath = "//*[@id='zebraStripe']/tbody/tr",
            UseAdditionalXpathIndex = true,
            Fields = new[]
            {
                new Tuple<string, string, string, ParseOptions.Extentions, string>
                    ("PracticeName", "/td/table/tbody/tr[1]/td[1]/text()[preceding-sibling::br]", null, ParseOptions.Extentions.DefaultIfNotFound, ""),
                new Tuple<string, string, string, ParseOptions.Extentions, string>
                    ("Address", "/td/table/tbody/tr[1]/td[1]/text()[preceding-sibling::br[2]]", null, ParseOptions.Extentions.DefaultIfNotFound, ""),
                new Tuple<string, string, string, ParseOptions.Extentions, string>
                    ("City", "/td/table/tbody/tr[1]/td[1]/text()[preceding-sibling::br[3]]", null, ParseOptions.Extentions.ReturnBeforeComma, ""),
                new Tuple<string, string, string, ParseOptions.Extentions, string>
                    ("State", "/td/table/tbody/tr[1]/td[1]/text()[preceding-sibling::br[3]]", null, ParseOptions.Extentions.ReturnPrev, ""),
                new Tuple<string, string, string, ParseOptions.Extentions, string>
                    ("Zip", "/td/table/tbody/tr[1]/td[1]/text()[preceding-sibling::br[3]]", null, ParseOptions.Extentions.ReturnLast, ""),
                new Tuple<string, string, string, ParseOptions.Extentions, string>
                    ("Email", "FIELD DOESN'T EXIST", null, ParseOptions.Extentions.DefaultIfNotFound, ""),
                new Tuple<string, string, string, ParseOptions.Extentions, string>
                    ("Phone", "/td/table/tbody/tr[1]/td[1]/text()[preceding-sibling::br[4]]", null, ParseOptions.Extentions.DefaultIfNotFound, ""),
                new Tuple<string, string, string, ParseOptions.Extentions, string>
                    ("DoctorsName", "/td/table/tbody/tr[1]/td[1]/span", null, ParseOptions.Extentions.DefaultIfNotFound, ""),
                new Tuple<string, string, string, ParseOptions.Extentions, string>
                    ("Specialty", "/td/table/tbody/tr[1]/td[2]/span[1]/u", null, ParseOptions.Extentions.DefaultIfNotFound, "")
            }
        };

        public MainForm()
        {
            InitializeComponent();
        }

        private void startStopScraperButton_Click(object sender, EventArgs e)
        {
            if (scrapForm == null)
            {
                scrapForm = new ScrapForm() { Text = "Scraper", ShowInTaskbar = true };
                scrapForm.Show();

                var del = new MethodInvoker(RunScraper);
                del.BeginInvoke(null, null);

                startStopScraperButton.Enabled = false;
                startStopScraperButton.Text = "Close scraper";
            }
            else
            {
                StopScrapThread(true);

                scrapForm.SetCanClose(true);
                scrapForm.Close();
                scrapForm = null;

                startStopScraperButton.Text = "Start scraper";
                buttonScrap.Enabled = false;
            }
        }

        private void StopScrapThread(bool forced)
        {
            if (scrapThread != null)
            {
                if (forced)
                    scrapThread.Abort();
                scrapThread = null;
                Invoke(new MethodInvoker(() => buttonScrap.Text = "Scrap"));
            }
        }

        private void RunScraper()
        {
            scraper = new ScrapHelper(radioButton1.Checked ? initialAddress1 : initialAddress2, true);
            var browser = scraper.s.GetBrowser();
            scrapForm.SetScraper(scraper.s);

            while (!browser.IsBrowserInitialized)
                Thread.Sleep(100);
            scrapForm.BrowserInitialized();
            CompleteStartingScraperProcess();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Process.GetCurrentProcess().Kill();
        }

        private void CompleteStartingScraperProcess()
        {
            Invoke(new MethodInvoker(() =>
            {
                BringToFront();
                TopMost = true;
                startStopScraperButton.Enabled = true;
                buttonScrap.Enabled = true;
            }));
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            Left = Screen.PrimaryScreen.Bounds.Width - Width - 20;
            Top = Screen.PrimaryScreen.Bounds.Height - Height - 80;
        }

        int zip;
        int distance;
        private void buttonScrap_Click(object sender, EventArgs e)
        {
            if (scrapThread == null)
            {
                int.TryParse(textBoxZip.Text, out zip);
                if (zip < 10000 || zip > 99999)
                {
                    MessageBox.Show("Нужен ZIP-код из 5 цифр.");
                    return;
                }

                int.TryParse(textBoxDistance.Text, out distance);
                if (distance < 5 || distance > 100)
                {
                    MessageBox.Show("Должна быть указана дистанция в пределах от 5 до 100 миль.");
                    return;
                }

                scrapThread = radioButton1.Checked ? new Thread(Scrap1Process) : new Thread(Scrap2Process);
                buttonScrap.Text = "Stop scrap";
                scrapThread.Start();
            }
            else
            {
                StopScrapThread(true);
            }
        }

        private string initialAddress1 = "https://www.deltadental.com/DentistSearch/DentistSearchController.ccl?Action=ViewDentistSearchForm";
        private string[] deltaDentalTypesPath = new[] { "//*[@id='in24']", "//*[@id='in25']" };
        private string textBoxZipCodePath1 = "//*[@id='in3']";
        private string radioDistanceTemplatePath = "//*[@id='in6']/../input[@value='{0}']";
        private string resultsCountPath = "//*[@id='resultSize']";
        private string buttonSearchPath1 = "//*[@id='in15']";
        private string prevTenResultsPath = "//*[@id='PageResults']/table/tbody/tr[1]/td[1]/a/strong";
        private string currentTenResultsPath = "//*[@id='PageResults']/table/tbody/tr[1]/td[2]/span/strong";
        private string nextTenResultsPath = "//*[@id='PageResults']/table/tbody/tr[1]/td[3]/a/strong";
        private string newSearchPath = "//a[@href='/DentistSearch/DentistSearchController.ccl?Action=ViewDentistSearchForm']";
        private void Scrap1Process()
        {
            try
            {
                List<Dentist> dentists = new List<Dentist>();

                for (int i = 0; i < 2; i++)
                {
                    scraper.ClickButton(deltaDentalTypesPath[i]);
                    scraper.SetInputValue(textBoxZipCodePath1, zip.ToString());
                    scraper.ClickButton(string.Format(radioDistanceTemplatePath, distance));
                    scraper.SetInputValue(resultsCountPath, "250");
                    scraper.ClickButton(buttonSearchPath1);
                    scraper.WaitAppearingElement(newSearchPath);

                    int resultsCount = 0;
                    string resultsCountString = scraper.Check(nextTenResultsPath)
                        ? scraper.GetString(nextTenResultsPath).Split('-').Last().Split('>').First()
                        : scraper.GetString(currentTenResultsPath).Split('-').Last();
                    int.TryParse(resultsCountString, out resultsCount);
                    int pagesCount = (resultsCount - 1) / 10 + (resultsCount == 0 ? 0 : 1);

                    if (scraper.Check(nextTenResultsPath))
                        for (int pageNumber = 1; ; pageNumber++)
                        {
                            if (pageNumber != pagesCount)
                                scraper.WaitAppearingElement(nextTenResultsPath);
                            dentists.AddRange(scraper.Parse<Dentist>(dentistsParseOptions));
                            if (pageNumber == pagesCount)
                                break;

                            string currentShowedIntervalText = scraper.GetString(currentTenResultsPath);
                            scraper.ClickButton(nextTenResultsPath);
                            scraper.WaitChangingTextOfElement(currentTenResultsPath, currentShowedIntervalText);
                            if (pageNumber > 1)
                                scraper.WaitAppearingElement(prevTenResultsPath);
                        }
                    dentists.AddRange(scraper.Parse<Dentist>(dentistsParseOptions));

                    scraper.Redirect(initialAddress1);
                }

                List<string> values = new List<string>();
                foreach (PropertyInfo propertyInfo in typeof(Dentist).GetProperties())
                    values.Add(propertyInfo.Name);
                string output = string.Join(",", values) + Environment.NewLine;
                foreach (Dentist den in dentists)
                {
                    values.Clear();
                    foreach (PropertyInfo propertyInfo in typeof(Dentist).GetProperties())
                        values.Add(propertyInfo.GetValue(den).ToString().Replace(",", ""));
                    output += string.Join(",", values) + Environment.NewLine;
                }
                File.WriteAllText("Output.csv", output);

                StopScrapThread(false);
            }
            catch (ThreadAbortException) { }
        }

        private string initialAddress2 = "https://connectiondental.com/provider/search";
        private string textBoxZipCodePath2 = "//*[@id='SearchCriteria_ZipCode']";
        private string textBoxDistancePath = "//*[@id='SearchCriteria_Distance']";
        private string buttonSearchPath2 = "//*[@id='SearchProviders']";
        private string textNoResultsPath = "//*[@id='dvDetailService']";
        private string textResultsCountPath = "//*[@id='dvDetailService']/span";
        private string nextPageButtonPath = "//*[.='»']";
        private string dentistStartPathTemplate = "//*[@id='dvDetailService']/div[{0}]/div[{1}]";
        private string doctorsNameAdditionalPath = "/div/div[1]/p/strong/a";
        private string addressLine1AdditionalPath = "/div/div[3]/p[1]";
        private string addressLine2AdditionalPath = "/div/div[3]/p[2]";
        private string cityStateZipAdditionalPath = "/div/div[3]/p[3]";
        private string phoneAdditionalPath = "/div/div[3]/p[4]";
        private string specialtyAdditionalPath = "/div/div[2]/p";
        private void Scrap2Process()
        {
            try
            {
                List<Dentist> dentists = new List<Dentist>();

                scraper.SetInputValue(textBoxZipCodePath2, zip.ToString());
                scraper.SetInputValue(textBoxDistancePath, distance.ToString());
                scraper.ClickButton(buttonSearchPath2);
                scraper.WaitAppearingAnyOfTwoElements(textNoResultsPath, textResultsCountPath);

                if (scraper.Check(textResultsCountPath))
                {
                    int resultsCount = int.Parse(scraper.GetString(textResultsCountPath).Trim().Split(' ')[3]);
                    for (int i = 0; dentists.Count < resultsCount; i++)
                    {
                        if (i != 0)
                        {
                            string currentLabelCountText = scraper.GetString(textResultsCountPath);
                            scraper.ClickButton(nextPageButtonPath);
                            scraper.WaitChangingTextOfElement(textResultsCountPath, currentLabelCountText);
                        }

                        for (int row = 1; row <= 3 && dentists.Count < resultsCount; row++)
                            for (int cell = 1; cell <= 3 && dentists.Count < resultsCount; cell++)
                            {
                                string dentistStartPath = string.Format(dentistStartPathTemplate, row, cell);
                                Dentist den = new Dentist();
                                den.DoctorsName = scraper.GetString(dentistStartPath + doctorsNameAdditionalPath);
                                den.Address = (scraper.GetString(dentistStartPath + addressLine1AdditionalPath) + " " +
                                    scraper.GetString(dentistStartPath + addressLine2AdditionalPath)).Trim();
                                string cityStateZip = scraper.GetString(dentistStartPath + cityStateZipAdditionalPath);
                                string[] cityStateZipComponents = cityStateZip.Split(' ');
                                den.City = cityStateZip.Split(',').First();
                                den.Zip = cityStateZipComponents.Last();
                                den.State = (cityStateZipComponents.Length >= 2) ? cityStateZipComponents[cityStateZipComponents.Length - 2] : "";
                                den.Email = "";
                                den.Phone = scraper.GetString(dentistStartPath + phoneAdditionalPath);
                                den.PracticeName = "";
                                den.Specialty = scraper.GetString(dentistStartPath + specialtyAdditionalPath);
                                dentists.Add(den);
                            }
                    }
                }

                List<string> values = new List<string>();
                foreach (PropertyInfo propertyInfo in typeof(Dentist).GetProperties())
                    values.Add(propertyInfo.Name);
                string output = string.Join(",", values) + Environment.NewLine;
                foreach (Dentist den in dentists)
                {
                    values.Clear();
                    foreach (PropertyInfo propertyInfo in typeof(Dentist).GetProperties())
                        values.Add(propertyInfo.GetValue(den).ToString().Replace(",", ""));
                    output += string.Join(",", values) + Environment.NewLine;
                }
                File.WriteAllText("Output.csv", output);

                StopScrapThread(false);
            }
            catch (ThreadAbortException) { }
        }
    }
}
