﻿using CefSharp.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DentistScraperVisual
{
    public partial class ScrapForm : Form
    {
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, int uFlags);
        Rectangle desktop = Screen.PrimaryScreen.WorkingArea;
        Rectangle savedBounds;
        bool devWindowOpened;
        Scraper.Scraper scraper;
        bool CanClose;

        public ScrapForm()
        {
            InitializeComponent();
        }

        public void SetCanClose(bool canClose)
        {
            CanClose = canClose;
        }

        public void SetScraper(Scraper.Scraper scraper)
        {
            this.scraper = scraper;

            Invoke(new MethodInvoker(() =>
            {
                ChromiumWebBrowser browser = (ChromiumWebBrowser)scraper.GetBrowser();
                panelBrowser.Controls.Add(browser);
                browser.Dock = DockStyle.Fill;
            }));
        }

        public void BrowserInitialized()
        {
            Invoke(new MethodInvoker(() =>
            {
                buttonDevTools.Enabled = true;
                buttonDevTools.PerformClick();
            }));
        }

        private void buttonDevTools_Click(object sender, EventArgs e)
        {
            if (!devWindowOpened)
            {
                savedBounds = Bounds;
                Bounds = new Rectangle(desktop.X, desktop.Y, 1032, desktop.Height);
                scraper.OpenDeveloperTools();
                IntPtr devWindow = IntPtr.Zero;
                for (int i = 0; i < 50 && devWindow == IntPtr.Zero; i++)
                {
                    Thread.Sleep(100);
                    Application.DoEvents();
                    devWindow = FindDevToolWindow();
                }
                SetWindowPos(devWindow, IntPtr.Zero, 1032, desktop.Y, desktop.Width - 1032, desktop.Height, 0);
            }
            else
            {
                Bounds = savedBounds;
                scraper.CloseDeveloperTools();
            }
            devWindowOpened = !devWindowOpened;
        }

        private IntPtr FindDevToolWindow()
        {
            IntPtr devWindow = FindWindow("CefBrowserWindow", "DevTools");
            if (devWindow != IntPtr.Zero)
                return devWindow;
            return devWindow = FindWindow("CefBrowserWindow", "chrome-devtools://devtools/inspector.html");
        }

        private void buttonBuffer1_Click(object sender, EventArgs e)
        {
            Clipboard.SetText("document.evaluate('', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE).singleNodeValue");
        }

        private void buttonBuffer2_Click(object sender, EventArgs e)
        {
            Clipboard.SetText("document.evaluate(\"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE).singleNodeValue");
        }

        private void ScrapForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !CanClose;
        }
    }
}
