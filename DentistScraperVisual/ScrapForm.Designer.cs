﻿namespace DentistScraperVisual
{
    partial class ScrapForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTop = new System.Windows.Forms.Panel();
            this.buttonBuffer1 = new System.Windows.Forms.Button();
            this.buttonDevTools = new System.Windows.Forms.Button();
            this.panelBrowser = new System.Windows.Forms.Panel();
            this.buttonBuffer2 = new System.Windows.Forms.Button();
            this.panelTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.Controls.Add(this.buttonBuffer2);
            this.panelTop.Controls.Add(this.buttonBuffer1);
            this.panelTop.Controls.Add(this.buttonDevTools);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(779, 27);
            this.panelTop.TabIndex = 1;
            // 
            // buttonBuffer1
            // 
            this.buttonBuffer1.Location = new System.Drawing.Point(91, 2);
            this.buttonBuffer1.Name = "buttonBuffer1";
            this.buttonBuffer1.Size = new System.Drawing.Size(88, 23);
            this.buttonBuffer1.TabIndex = 2;
            this.buttonBuffer1.Text = "Buffer \' \'";
            this.buttonBuffer1.UseVisualStyleBackColor = true;
            this.buttonBuffer1.Click += new System.EventHandler(this.buttonBuffer1_Click);
            // 
            // buttonDevTools
            // 
            this.buttonDevTools.Enabled = false;
            this.buttonDevTools.Location = new System.Drawing.Point(2, 2);
            this.buttonDevTools.Name = "buttonDevTools";
            this.buttonDevTools.Size = new System.Drawing.Size(88, 23);
            this.buttonDevTools.TabIndex = 1;
            this.buttonDevTools.Text = "Dev tools";
            this.buttonDevTools.UseVisualStyleBackColor = true;
            this.buttonDevTools.Click += new System.EventHandler(this.buttonDevTools_Click);
            // 
            // panelBrowser
            // 
            this.panelBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBrowser.Location = new System.Drawing.Point(0, 27);
            this.panelBrowser.Name = "panelBrowser";
            this.panelBrowser.Size = new System.Drawing.Size(779, 419);
            this.panelBrowser.TabIndex = 2;
            // 
            // buttonBuffer2
            // 
            this.buttonBuffer2.Location = new System.Drawing.Point(179, 2);
            this.buttonBuffer2.Name = "buttonBuffer2";
            this.buttonBuffer2.Size = new System.Drawing.Size(88, 23);
            this.buttonBuffer2.TabIndex = 3;
            this.buttonBuffer2.Text = "Buffer \" \"";
            this.buttonBuffer2.UseVisualStyleBackColor = true;
            this.buttonBuffer2.Click += new System.EventHandler(this.buttonBuffer2_Click);
            // 
            // ScrapForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 446);
            this.Controls.Add(this.panelBrowser);
            this.Controls.Add(this.panelTop);
            this.Name = "ScrapForm";
            this.Text = "ScrapForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ScrapForm_FormClosing);
            this.panelTop.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Button buttonDevTools;
        private System.Windows.Forms.Button buttonBuffer1;
        private System.Windows.Forms.Panel panelBrowser;
        private System.Windows.Forms.Button buttonBuffer2;
    }
}