﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DentistScraper
{
    /// <summary>
    /// Interface for dental clinic scraper classes
    /// </summary>
    public interface IDentistScraper
    {
        /// <summary>
        /// Retrieves list of available doctors/clinics
        /// </summary>
        /// <param name="zip">Zip code of city</param>
        /// <param name="distance">Maximum distance from city, mile. Must be one of values, available in list.</param>
        IEnumerable<Dentist> ScrapDentists(int zip, int distance);
    }
}
