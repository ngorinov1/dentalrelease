﻿using Scraper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace DentistScraper
{
    public class DeltaDental : BaseScraper
    {
        public DeltaDental(Action<string> logger)
            : base(logger, "https://www.deltadental.com/DentistSearch/DentistSearchController.ccl?Action=ViewDentistSearchForm")
        {
        }

        static ParseOptions dentistsParseOptions = new ParseOptions
        {
            BlockPath = "//*[@id='zebraStripe']/tbody/tr",
            UseAdditionalXpathIndex = true,
            Fields = new[]
            {
                new Tuple<string, string, string, ParseOptions.Extentions, string>
                    ("PracticeName", "/td/table/tbody/tr[1]/td[1]/text()[preceding-sibling::br]", null, ParseOptions.Extentions.DefaultIfNotFound, ""),
                new Tuple<string, string, string, ParseOptions.Extentions, string>
                    ("Address", "/td/table/tbody/tr[1]/td[1]/text()[preceding-sibling::br[2]]", null, ParseOptions.Extentions.DefaultIfNotFound, ""),
                new Tuple<string, string, string, ParseOptions.Extentions, string>
                    ("City", "/td/table/tbody/tr[1]/td[1]/text()[preceding-sibling::br[3]]", null, ParseOptions.Extentions.ReturnBeforeComma, ""),
                new Tuple<string, string, string, ParseOptions.Extentions, string>
                    ("State", "/td/table/tbody/tr[1]/td[1]/text()[preceding-sibling::br[3]]", null, ParseOptions.Extentions.ReturnPrev, ""),
                new Tuple<string, string, string, ParseOptions.Extentions, string>
                    ("Zip", "/td/table/tbody/tr[1]/td[1]/text()[preceding-sibling::br[3]]", null, ParseOptions.Extentions.ReturnLast, ""),
                new Tuple<string, string, string, ParseOptions.Extentions, string>
                    ("Email", "FIELD DOESN'T EXIST", null, ParseOptions.Extentions.DefaultIfNotFound, ""),
                new Tuple<string, string, string, ParseOptions.Extentions, string>
                    ("Phone", "/td/table/tbody/tr[1]/td[1]/text()[preceding-sibling::br[4]]", null, ParseOptions.Extentions.DefaultIfNotFound, ""),
                new Tuple<string, string, string, ParseOptions.Extentions, string>
                    ("DoctorsName", "/td/table/tbody/tr[1]/td[1]/span", null, ParseOptions.Extentions.DefaultIfNotFound, ""),
                new Tuple<string, string, string, ParseOptions.Extentions, string>
                    ("Specialty", "/td/table/tbody/tr[1]/td[2]/span[1]/u", null, ParseOptions.Extentions.DefaultIfNotFound, "")
            }
        };

        private string[] deltaDentalTypesPath = new[] { "//*[@id='in24']", "//*[@id='in25']" };
        private string textBoxZipCodePath = "//*[@id='in3']";
        private string radioDistanceTemplatePath = "//*[@id='in6']/../input[@value='{0}']";
        private string resultsCountPath = "//*[@id='resultSize']";
        private string buttonSearchPath = "//*[@id='in15']";
        private string prevTenResultsPath = "//*[@id='PageResults']/table/tbody/tr[1]/td[1]/a/strong";
        private string currentTenResultsPath = "//*[@id='PageResults']/table/tbody/tr[1]/td[2]/span/strong";
        private string nextTenResultsPath = "//*[@id='PageResults']/table/tbody/tr[1]/td[3]/a/strong";
        private string newSearchPath = "//a[@href='/DentistSearch/DentistSearchController.ccl?Action=ViewDentistSearchForm']";
        public override IEnumerable<Dentist> ScrapDentists(int zip, int distance)
        {
            List<Dentist> dentists = new List<Dentist>();
            for (int i = 0; i < 2; i++)
            {
                scraper.Redirect(initialAddress);
                scraper.WaitAppearingElement(textBoxZipCodePath);
                Thread.Sleep(500);
                scraper.ClickButton(deltaDentalTypesPath[i]);
                scraper.SetInputValue(textBoxZipCodePath, zip.ToString());
                scraper.ClickButton(string.Format(radioDistanceTemplatePath, distance));
                scraper.SetInputValue(resultsCountPath, "250");
                scraper.ClickButton(buttonSearchPath);
                scraper.WaitAppearingElement(newSearchPath);

                int resultsCount = 0;
                string resultsCountString = scraper.Check(nextTenResultsPath)
                    ? scraper.GetString(nextTenResultsPath).Split('-').Last().Split('>').First()
                    : scraper.GetString(currentTenResultsPath).Split('-').Last();
                int.TryParse(resultsCountString, out resultsCount);
                int pagesCount = (resultsCount - 1) / 10 + (resultsCount == 0 ? 0 : 1);
                Log(string.Format("Found {0} results on {1} pages for {2}", resultsCount, pagesCount,
                    (i == 0 ? "Delta Dental PPO" : "Delta Dental Premier")));

                if (resultsCount > 0)
                    Log(string.Format("Page {0}/{1} loaded", 1, pagesCount));
                if (scraper.Check(nextTenResultsPath))
                    for (int pageNumber = 1; ; pageNumber++)
                    {
                        if (pageNumber != pagesCount)
                            scraper.WaitAppearingElement(nextTenResultsPath);
                        dentists.AddRange(scraper.Parse<Dentist>(dentistsParseOptions));
                        if (pageNumber == pagesCount)
                            break;

                        string currentShowedIntervalText = scraper.GetString(currentTenResultsPath);
                        scraper.ClickButton(nextTenResultsPath);
                        scraper.WaitChangingTextOfElement(currentTenResultsPath, currentShowedIntervalText);
                        if (pageNumber > 1)
                            scraper.WaitAppearingElement(prevTenResultsPath);
                        Log(string.Format("Page {0}/{1} loaded", pageNumber + 1, pagesCount));
                    }
                dentists.AddRange(scraper.Parse<Dentist>(dentistsParseOptions));
            }

            return dentists;
        }
    }
}
