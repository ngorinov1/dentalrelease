﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace DentistScraper
{
    public class GehaDental : BaseScraper
    {
        public GehaDental(Action<string> logger)
            : base(logger, "https://connectiondental.com/provider/search")
        {
        }

        private string textBoxZipCodePath = "//*[@id='SearchCriteria_ZipCode']";
        private string textBoxDistancePath = "//*[@id='SearchCriteria_Distance']";
        private string buttonSearchPath = "//*[@id='SearchProviders']";
        private string textNoResultsPath = "//*[@id='dvDetailService']";
        private string textResultsCountPath = "//*[@id='dvDetailService']/span";
        private string nextPageButtonPath = "//*[.='»']";
        private string dentistStartPathTemplate = "//*[@id='dvDetailService']/div[{0}]/div[{1}]";
        private string doctorsNameAdditionalPath = "/div/div[1]/p/strong/a";
        private string addressLine1AdditionalPath = "/div/div[3]/p[1]";
        private string addressLine2AdditionalPath = "/div/div[3]/p[2]";
        private string cityStateZipAdditionalPath = "/div/div[3]/p[3]";
        private string phoneAdditionalPath = "/div/div[3]/p[4]";
        private string specialtyAdditionalPath = "/div/div[2]/p";

        public override IEnumerable<Dentist> ScrapDentists(int zip, int distance)
        {
            scraper.Redirect(initialAddress);
            scraper.WaitAppearingElement(textBoxZipCodePath);
            Thread.Sleep(500);
            scraper.SetInputValue(textBoxZipCodePath, zip.ToString());
            scraper.SetInputValue(textBoxDistancePath, distance.ToString());
            scraper.ClickButton(buttonSearchPath);
            scraper.WaitAppearingAnyOfTwoElements(textNoResultsPath, textResultsCountPath);

            List<Dentist> dentists = new List<Dentist>();
            if (scraper.Check(textResultsCountPath))
            {
                int resultsCount = int.Parse(scraper.GetString(textResultsCountPath).Trim().Split(' ')[3]);
                int pagesCount = (resultsCount - 1) / 9 + (resultsCount == 0 ? 0 : 1);
                Log(string.Format("Found {0} results on {1} pages", resultsCount, pagesCount));

                for (int i = 0; dentists.Count < resultsCount; i++)
                {
                    if (i != 0)
                    {
                        string currentLabelCountText = scraper.GetString(textResultsCountPath);
                        scraper.ClickButton(nextPageButtonPath);
                        scraper.WaitChangingTextOfElement(textResultsCountPath, currentLabelCountText);
                    }

                    Log(string.Format("Page {0}/{1} loaded", i + 1, pagesCount));
                    for (int row = 1; row <= 3 && dentists.Count < resultsCount; row++)
                        for (int cell = 1; cell <= 3 && dentists.Count < resultsCount; cell++)
                        {
                            string dentistStartPath = string.Format(dentistStartPathTemplate, row, cell);
                            Dentist den = new Dentist();
                            den.DoctorsName = scraper.GetString(dentistStartPath + doctorsNameAdditionalPath);
                            den.Address = (scraper.GetString(dentistStartPath + addressLine1AdditionalPath) + " " +
                                scraper.GetString(dentistStartPath + addressLine2AdditionalPath)).Trim();
                            string cityStateZip = scraper.GetString(dentistStartPath + cityStateZipAdditionalPath);
                            string[] cityStateZipComponents = cityStateZip.Split(' ');
                            den.City = cityStateZip.Split(',').First();
                            den.Zip = cityStateZipComponents.Last();
                            den.State = (cityStateZipComponents.Length >= 2) ? cityStateZipComponents[cityStateZipComponents.Length - 2] : "";
                            den.Email = "";
                            den.Phone = scraper.GetString(dentistStartPath + phoneAdditionalPath);
                            den.PracticeName = "";
                            den.Specialty = scraper.GetString(dentistStartPath + specialtyAdditionalPath);
                            dentists.Add(den);
                        }
                }
            }

            return dentists;
        }
    }
}
