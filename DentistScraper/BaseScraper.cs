﻿using CefSharp;
using Scraper;
using System;
using System.Collections.Generic;
using System.Threading;

namespace DentistScraper
{
    public abstract class BaseScraper
    {
        protected Action<string> logger;
        protected ScrapHelper scraper;
        protected string initialAddress;

        public BaseScraper(Action<string> logger, string initialAddress)
        {
            this.logger = logger;
            this.initialAddress = initialAddress;
            scraper = new ScrapHelper(initialAddress);
            IWebBrowser browser = scraper.s.GetBrowser();
            while (!browser.IsBrowserInitialized || !browser.CanExecuteJavascriptInMainFrame)
                Thread.Sleep(100);
        }

        public void ScrapDentistsMulti(string inputCsv, string outputCsv)
        {
            List<ScrapParameters> scrapParametersList = CsvProcessor.LoadCsv<ScrapParameters>("Input.csv");
            foreach (ScrapParameters parameters in scrapParametersList)
            {
                try
                {
                    Log(string.Format("Scraping zipcode {0} and miles {1}", parameters.Zipcode, parameters.Miles));
                    IEnumerable<Dentist> dentists = ScrapDentists(int.Parse(parameters.Zipcode), int.Parse(parameters.Miles));
                    CsvProcessor.SaveListToCsv<Dentist>(dentists, "OutputDeltaDentalMulti.csv");
                }
                catch (Exception ex)
                {
                    Log(ex.ToString());
                }
            }
        }

        /// <summary>
        /// Retrieves list of available doctors/clinics
        /// </summary>
        /// <param name="zip">Zip code of city</param>
        /// <param name="distance">Maximum distance from city, mile. Must be one of values, available in list.</param>
        public abstract IEnumerable<Dentist> ScrapDentists(int zip, int distance);

        protected void Log(string text)
        {
            if (logger != null)
                logger(text);
        }
    }
}
