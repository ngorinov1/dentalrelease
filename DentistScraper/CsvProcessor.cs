﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace DentistScraper
{
    public static class CsvProcessor
    {
        public static void SaveListToCsv<T>(IEnumerable<T> list, string filename)
        {
            List<string> values = new List<string>();

            string output = "";
            if (!File.Exists(filename) || new FileInfo(filename).Length == 0)
            {
                foreach (PropertyInfo propertyInfo in typeof(T).GetProperties())
                    values.Add(propertyInfo.Name);
                output = string.Join(",", values) + Environment.NewLine;
            }

            foreach (T den in list)
            {
                values.Clear();
                foreach (PropertyInfo propertyInfo in typeof(T).GetProperties())
                    values.Add(propertyInfo.GetValue(den).ToString().Replace(",", ""));
                output += string.Join(",", values) + Environment.NewLine;
            }

            File.AppendAllText(filename, output);
        }

        public static List<T> LoadCsv<T>(string inputCsv) where T : new()
        {
            List<T> result = new List<T>();

            string[] csvLines = File.ReadAllLines(inputCsv);
            if (csvLines.Length == 0)
                return result;

            string[] columns = csvLines[0].Split(',');
            for (int i = 1; i < csvLines.Length; i++)
            {
                string[] fields = csvLines[i].Split(',');
                if (fields.Length < columns.Length)
                    continue;

                T record = new T();
                for (int j = 0; j < columns.Length; j++)
                {
                    PropertyInfo fieldInfo = record.GetType().GetProperty(columns[j]);
                    fieldInfo.SetValue(record, fields[j], null);
                }
                result.Add(record);
            }

            return result;
        }
    }
}
