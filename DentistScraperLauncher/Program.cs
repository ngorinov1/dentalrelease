﻿using CefSharp;
using DentistScraper;
using System;
using System.IO;
using System.Reflection;

namespace DentistScraperLauncher
{
    class Program
    {
        private static string LogsDirectoryPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "logs");

        static void Main(string[] args)
        {
            // ============================================= DELTA DENTAL MULTI ===============================================
            Log("Testing Delta Dental csv multi scrapping...");
            BaseScraper deltaDental = new DeltaDental(Log);
            deltaDental.ScrapDentistsMulti("Input.csv", "OutputDeltaDentalMulti.csv");

            // ============================================== GEHA DENTAL MULTI ===============================================
            //Log("Testing Delta Dental csv multi scrapping...");
            //BaseScraper gehaDental = new GehaDental(Log);
            //gehaDental.ScrapDentistsMulti("Input.csv", "OutputGehaDentalMulti.csv");

            // ================================================ DELTA DENTAL ==================================================
            //Log("Testing Delta Dental...");
            //deltaDental = new DeltaDental(Log);
            //IEnumerable<Dentist> deltaDentists = deltaDental.ScrapDentists(10001, 5);

            //string deltaDentalFilename = "OutputDeltaDental.csv";
            //CsvProcessor.SaveListToCsv<Dentist>(deltaDentists, deltaDentalFilename, false);
            //Log(string.Format("Delta Dental results successfully exported into {0} file", deltaDentalFilename));

            // ================================================ GEHA DENTAL ===================================================
            //Log("Testing Geha Dental...");
            //IDentistScraper gehaDental = new GehaDental(Log);
            //IEnumerable<Dentist> gehaDentists = gehaDental.ScrapDentists(12345, 5);

            //string gehaDentalFilename = "OutputGehaDental.csv";
            //CsvProcessor.SaveListToCsv<Dentist>(gehaDentists, gehaDentalFilename, false);
            //Log(string.Format("Geha Dental results successfully exported into {0} file", gehaDentalFilename));

            // =================================================================================================================
            Cef.Shutdown();
            Log("Press any key to exit...");
            Console.ReadKey();
        }

        public static void Log(string text)
        {
            string logText = DateTime.Now.ToString("[dd.MM.yyyy HH:mm:ss] ") + text;
            Console.WriteLine(logText);
            string path = Path.Combine(LogsDirectoryPath, "Log.txt");
            string writingText = (logText.Replace("\n", "\r\n") + "\r\n");
            if (!Directory.Exists(LogsDirectoryPath))
                Directory.CreateDirectory(LogsDirectoryPath);
            File.AppendAllText(path, writingText);
        }
    }
}
