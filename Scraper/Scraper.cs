﻿using CefSharp;
using CefSharp.OffScreen;
using CefSharp.WinForms;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.Threading.Tasks;
using System.Linq;

namespace Scraper
{
    public class Scraper
    {
        IWebBrowser browser;
        string BackLocation;
        bool Visible;

        public IWebBrowser GetBrowser()
        {
            return browser;
        }

        public static Task<bool> LoadPageAsync(IWebBrowser browser, string address = null)
        {
            var tcs = new TaskCompletionSource<bool>();

            EventHandler<LoadingStateChangedEventArgs> handler = null;
            handler = (sender, args) =>
            {
                //Wait for while page to finish loading not just the first frame
                if (!args.IsLoading)
                {
                    browser.LoadingStateChanged -= handler;
                    tcs.TrySetResult(true);
                }
            };

            browser.LoadingStateChanged += handler;

            if (!string.IsNullOrEmpty(address))
            {
                browser.Load(address);
            }
            return tcs.Task;
        }

        private HtmlDocument FrozenHtmlDoc = null;
        public async Task FreezeHtml(string html = null)
        {
            FrozenHtmlDoc = await GetHtmlDoc(html);
        }

        public void UnfreezeHtml()
        {
            FrozenHtmlDoc = null;
        }

        private async Task<HtmlDocument> GetHtmlDoc(string source = null)
        {
            if (FrozenHtmlDoc == null)
            {
                HtmlDocument doc = new HtmlDocument();
                string html = ((source == null) ? await GetHtml() : source);
                doc.LoadHtml(html);
                return doc;
            }
            else
            {
                return FrozenHtmlDoc;
            }
        }

        public static Task<bool> RedirectAsync(IWebBrowser browser, string address = null)
        {
            var tsc = new TaskCompletionSource<bool>();
            EventHandler<LoadingStateChangedEventArgs> handler = null;
            handler = (sender, args) =>
            {
                if (args.IsLoading)
                {
                    browser.LoadingStateChanged -= handler;
                    tsc.TrySetResult(true);
                }
            };

            browser.LoadingStateChanged += handler;

            Task.Delay(500).Wait();

            if (!string.IsNullOrEmpty(address))
            {
                string script = string.Format("self.location = '{0}';", address);
                browser.EvaluateScriptAsync(script);
                Task.Delay(100).Wait();
            }
            return tsc.Task;
        }

        public static Task<bool> DoAndWaitAsync(IWebBrowser browser, Action<IWebBrowser, object> Work, object WorkArguments)
        {
            var tsc = new TaskCompletionSource<bool>();
            EventHandler<LoadingStateChangedEventArgs> handler = null;
            handler = (sender, args) =>
            {
                if (args.IsLoading)
                {
                    browser.LoadingStateChanged -= handler;
                    tsc.TrySetResult(true);
                }
            };

            browser.LoadingStateChanged += handler;

            Work.BeginInvoke(browser, WorkArguments, null, null);

            return tsc.Task;
        }

        public static async Task<Scraper> CreateScraper(string InitUrl, CefSharp.RequestContext context, bool visible = false)
        {
            Scraper src = new Scraper();
            if (!string.IsNullOrEmpty(InitUrl))
            {
                src.BackLocation = InitUrl;
                if (context != null)
                    await src.InitScraper(InitUrl, context, visible);
                else
                    await src.InitScraper(InitUrl, visible);
            }
            return src;
        }

        public async Task InitScraper(string InitUrl, bool visible)
        {
            await InitScraper(InitUrl, GetContext(), visible);
        }

        public static RequestContext GetContext()
        {
            var requestContextSettings = new RequestContextSettings { CachePath = null };
            var requestContext = new RequestContext(requestContextSettings);
            return requestContext;
        }

        public async Task InitScraper(string InitUrl, RequestContext requestContext, bool visible)
        {
            Visible = visible;
            if (visible)
            {
                browser = new CefSharp.WinForms.ChromiumWebBrowser(InitUrl);
                //browser.RequestContext = requestContext;
                browser.LoadingStateChanged += browser_LoadingStateChanged;
            }
            else
            {
                var browserSettings = new BrowserSettings();
                browser = new CefSharp.OffScreen.ChromiumWebBrowser(InitUrl, browserSettings, requestContext);
                browser.LoadingStateChanged += browser_LoadingStateChanged;
                await LoadPageAsync(browser);
            }
        }

        bool m_loadingStateChanged;

        public bool IsLoadingStateChanged
        {
            get
            {
                if (m_loadingStateChanged)
                {
                    m_loadingStateChanged = false;
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        void browser_LoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        {
            m_loadingStateChanged = true;
        }

        public void SetPageSize(Size size)
        {
            if (!Visible)
                ((CefSharp.OffScreen.ChromiumWebBrowser)browser).Size = size;
            else
                ((CefSharp.WinForms.ChromiumWebBrowser)browser).Size = size;
        }

        public async Task SetInputValue(string XPath, string value)
        {
            string script = string.Format(
                "var XPathVar = getElementByXpath(\"{0}\"); \n" +
                "XPathVar.value = \"{1}\";  \n" +
                "function getElementByXpath(path) {{ \n" +
                "    return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue; \n" +
                "}} \n", XPath, value);
            await browser.EvaluateScriptAsync(script);
        }

        public async Task SetInputValueInsideIframe(string iFramePath, string id, string value)
        {
            string script = string.Format(
                "var XPathVar = getElementByXpathInsideIframe(\"{0}\", \"{1}\"); \n" +
                "XPathVar.value = \"{2}\";  \n" +
                "function getElementByXpathInsideIframe(path, id) {{ \n" +
                "    return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.contentWindow.document.getElementById(id); \n" +
                "}} \n", iFramePath, id, value);
            await browser.EvaluateScriptAsync(script);
        }

        public async Task ClickButton(string XPath)
        {
            string script = string.Format(
                "var sendButton = getElementByXpath(\"{0}\"); \n" +
                "sendButton.click(); \n" +
                "function getElementByXpath(path) {{ \n" +
                "    return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue; \n" +
                "}} \n", XPath);
            await browser.EvaluateScriptAsync(script);
        }

        public async Task ClickButtonInsideIframe(string iFramePath, string id)
        {
            string script = string.Format(
                "var sendButton = getElementByXpathInsideIframe(\"{0}\", \"{1}\"); \n" +
                "sendButton.click(); \n" +
                "function getElementByXpathInsideIframe(path, id) {{ \n" +
                "    return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.contentWindow.document.getElementById(id); \n" +
                "}} \n", iFramePath, id);
            await browser.EvaluateScriptAsync(script);
        }

        public async Task MakeEvent(string xPath, string eventType)
        {
            string script = string.Format(
                "var element = document.evaluate(\"{0}\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue; \n" +
                "var o = document.createEvent('UIEvent'); \n" +
                "o.initUIEvent('{1}', true, true, window, 1); \n" +
                "o.keyCode = 65; \n" +
                "element.dispatchEvent(o);", xPath, eventType);
            await browser.EvaluateScriptAsync(script);
        }

        public async Task MakeEventInsideIframe(string iFramePath, string id, string eventType)
        {
            string script = string.Format(
                "var element = document.evaluate(\"{0}\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.contentWindow.document.getElementById(\"{1}\"); \n" +
                "var o = document.createEvent('UIEvent'); \n" +
                "o.initUIEvent('{2}', true, true, window, 1); \n" +
                "o.keyCode = 65; \n" +
                "element.dispatchEvent(o);", iFramePath, id, eventType);
            await browser.EvaluateScriptAsync(script);
        }

        public async Task ScrollDownPage()
        {
            //await DoAndWaitAsync(browser, new Action<IWebBrowser,object>(async (browser1, param) => {
            //    string script = "window.scrollTo(0, document.body.scrollHeight);";
            //    await  browser1.EvaluateScriptAsync(script);
            //}), null);

            string script = "window.scrollTo(0, document.body.scrollHeight);";
            await browser.EvaluateScriptAsync(script);
        }

        public async Task GoBack()
        {
            string script = string.Format("self.location = '{0}';", BackLocation);
            await browser.EvaluateScriptAsync(script);
        }

        public async Task<object> GetObjectValue(string objectName)
        {
            var result = await browser.EvaluateScriptAsync(objectName);
            return result.Result;
        }

        public async Task DeleteElement(string xPath)
        {
            await GetObjectValue("document.evaluate(\"" + xPath + "\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE).singleNodeValue.remove()");
        }

        public async Task<bool> Check(string xPath)
        {
            var ret = await GetObjectValue("document.evaluate(\"" + xPath.Replace("\"", "'") +
                "\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE).singleNodeValue.id");

            return (ret != null);
        }

        public async Task<bool> CheckElementInIframe(string iFramePath, string id)
        {
            var ret = await GetObjectValue("document.evaluate(\"" + iFramePath.Replace("\"", "'") +
                "\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE).singleNodeValue.contentWindow.document.getElementById(\"" + id.Replace("\"", "'") + "\").id");

            return (ret != null);
        }

        public async Task<bool> Check2(string xPath)
        {
            HtmlDocument doc = await GetHtmlDoc();
            var ret = doc.DocumentNode.SelectNodes(xPath);

            return (ret != null);
        }

        public async Task<bool> CheckElementInLastIframe(string xPath)
        {
            HtmlDocument doc = new HtmlDocument();
            string html = await GetLastIframeHtml();
            doc.LoadHtml(html);

            return (doc.DocumentNode.SelectNodes(xPath) != null);
        }

        public async Task<string> GetStringInLastIframe(string xPath)
        {
            HtmlDocument doc = new HtmlDocument();
            string html = await GetLastIframeHtml();
            doc.LoadHtml(html);

            HtmlNodeCollection blocks = doc.DocumentNode.SelectNodes(xPath);
            return ((blocks != null) && (blocks.Count > 0)) ? DeHtml(blocks[0].InnerText) : "";
        }

        public async Task<int> GetCount(string xPath)
        {
            HtmlDocument doc = await GetHtmlDoc();
            HtmlNodeCollection nodes = doc.DocumentNode.SelectNodes(xPath);

            return (nodes == null) ? 0 : nodes.Count;
        }

        public async Task<string> GetAttribute(string xPath, string attribute)
        {
            HtmlDocument doc = await GetHtmlDoc();
            HtmlNodeCollection blocks = doc.DocumentNode.SelectNodes(xPath);
            if ((blocks != null) && (blocks.Count > 0) && !blocks[0].Attributes.Contains(attribute))
                return null;
            return ((blocks != null) && (blocks.Count > 0) && (blocks[0].Attributes[attribute] != null)) ?
                blocks[0].Attributes[attribute].Value : "";
        }

        public async Task<string> GetString(string xPath)
        {
            HtmlDocument doc = await GetHtmlDoc();
            HtmlNodeCollection blocks = doc.DocumentNode.SelectNodes(xPath);
            return ((blocks != null) && (blocks.Count > 0)) ? DeHtml(blocks[0].InnerText) : "";
        }

        public async Task<List<string>> GetElementsAttributes(string xPath, string attributeName)
        {
            HtmlDocument doc = await GetHtmlDoc();
            HtmlNodeCollection blocks = doc.DocumentNode.SelectNodes(xPath);

            List<string> result = new List<string>();
            if (blocks != null)
                foreach (HtmlNode node in blocks)
                {
                    if (node.Attributes[attributeName] != null)
                        result.Add((node.Attributes[attributeName].Value));
                }
            return result;
        }

        public async Task<List<string>> GetStrings(string xPath)
        {
            HtmlDocument doc = await GetHtmlDoc();
            HtmlNodeCollection blocks = doc.DocumentNode.SelectNodes(xPath);

            List<string> result = new List<string>();
            if (blocks != null)
                foreach (HtmlNode node in blocks)
                    result.Add((node.InnerText != null) ? DeHtml(node.InnerText) : "");
            return result;
        }

        public async Task Refresh()
        {
            BackLocation = browser.Address;
            string script = "location.reload();";
            await browser.EvaluateScriptAsync(script);
        }

        public async Task Redirect(string Url)
        {
            //await RedirectAsync(browser, Url);
            //string script = string.Format("alert('{0}');", Url);

            BackLocation = browser.Address;
            string script = string.Format("self.location = '{0}';", Url);
            await browser.EvaluateScriptAsync(script);
        }

        public async Task<string> GetHtml()
        {
            return await browser.GetSourceAsync();
            //return System.IO.File.ReadAllText("C:\\temp\\result.html");
        }

        public async Task<string> GetIFrameHtml(IFrame iFrame)
        {
            return await iFrame.GetSourceAsync();
        }

        public async Task<string> GetLastIframeHtml()
        {
            int framesCount = browser.GetBrowser().GetFrameCount();
            long lastIframeIdentifier = browser.GetBrowser().GetFrameIdentifiers()[framesCount - 1];
            string res = await browser.GetBrowser().GetFrame(lastIframeIdentifier).GetSourceAsync();
            return res;
        }

        public async Task<Image> GetScreenShot()
        {
            if (!Visible)
                return await ((CefSharp.OffScreen.ChromiumWebBrowser)browser).ScreenshotAsync();
            return null;
        }

        public void OpenDeveloperTools()
        {
            browser.ShowDevTools();
        }

        public void CloseDeveloperTools()
        {
            browser.CloseDevTools();
        }

        public async Task<List<T>> Parse<T>(ParseOptions options) where T : new()
        {
            HtmlDocument doc = await GetHtmlDoc();
            HtmlNodeCollection blocks = doc.DocumentNode.SelectNodes(options.BlockPath);
            int elementsCount = (blocks == null) ? 0 : blocks.Count;
            List<T> parsedData = new List<T>();
            for (int i = 0; i < elementsCount; i++)
                parsedData.Add(new T());

            foreach (Tuple<string, string, string, ParseOptions.Extentions, string> rule in options.Fields)
            {
                HtmlNodeCollection fieldNodes = null;
                if (!options.UseAdditionalXpathIndex)
                    fieldNodes = doc.DocumentNode.SelectNodes(options.BlockPath + rule.Item2);
                for (int i = 0; i < elementsCount; i++)
                {
                    string fieldValue = null;
                    try
                    {
                        if (options.UseAdditionalXpathIndex)
                            fieldNodes = doc.DocumentNode.SelectNodes(options.BlockPath + ("[" + (i + 1) + "]") + rule.Item2);
                        if ((fieldNodes != null) && (fieldNodes.Count > 0))
                        {
                            int index = (options.UseAdditionalXpathIndex ? 0 : i);
                            if (rule.Item3 == null)
                                fieldValue = fieldNodes[index].InnerText;
                            //Encoding.GetEncoding("UTF-8").GetString(Encoding.GetEncoding(1251).GetBytes(fieldNodes[i].InnerText));
                            else if (rule.Item3 == "")
                                fieldValue = fieldNodes[index].InnerHtml;
                            else
                                fieldValue = (fieldNodes[index].Attributes[rule.Item3] == null) ? null : fieldNodes[index].Attributes[rule.Item3].Value;
                            if (fieldValue != null)
                                fieldValue = DeHtml(fieldValue).Trim();
                        }

                        if (rule.Item4 == ParseOptions.Extentions.ReturnBeforeComma)
                            fieldValue = (fieldValue == null) ? "" : fieldValue.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).First();
                        if (rule.Item4 == ParseOptions.Extentions.ReturnLast)
                            fieldValue = (fieldValue == null) ? "" : fieldValue.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Last();
                        if (rule.Item4 == ParseOptions.Extentions.ReturnPrev)
                        {
                            string[] r = (fieldValue == null) ? new string[0] : fieldValue.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            fieldValue = (r.Length >= 2) ? r[r.Length - 2] : "";
                        }
                    }
                    catch
                    {
                        if (rule.Item4 == ParseOptions.Extentions.DefaultIfNotFound)
                            fieldValue = rule.Item5;
                        if (rule.Item4 == ParseOptions.Extentions.ExceptionIfNotFound)
                            throw;
                    }

                    PropertyInfo fieldInfo = parsedData[i].GetType().GetProperty(rule.Item1);
                    fieldInfo.SetValue(parsedData[i], fieldValue, null);
                }
            }

            return parsedData;
        }

        string DeHtml(string source)
        {
            if (source.Contains("&amp;") || source.Contains("&nbsp;") || source.Contains("&lt;") || source.Contains("&gt;"))
                return source.Replace("&amp;", "&").Replace("&nbsp;", " ").Replace("&lt;", "<").Replace("&gt;", ">");
            else
                return source;
        }

        public void Shutdown()
        {
            try
            {
                browser.GetBrowser().Dispose();
            }
            catch { }
            try
            {
                browser.Dispose();
            }
            catch { }
        }
    }

    public class ParseOptions
    {
        public enum Extentions
        {
            NullIfNotFound,
            DefaultIfNotFound,
            ExceptionIfNotFound,
            ReturnBeforeComma,
            ReturnPrev,
            ReturnLast
        };

        public string BlockPath { get; set; }

        public bool UseAdditionalXpathIndex { get; set; }
        public Tuple<string, string, string, Extentions, string>[] Fields;
    }
}
