﻿using System;

namespace Scraper
{
    public class ScrapingException : Exception
    {
        public ScrapingException(string message)
            : base(message)
        {
        }
    }
}
