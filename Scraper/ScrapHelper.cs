﻿using CefSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Threading;

namespace Scraper
{
    public class ScrapHelper
    {
        static string LogsDirectoryPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "logs");
        public Scraper s;

        //public ScrapHelper(int i)
        //{
        //    s = new Scraper();
        //}

        public ScrapHelper(string url, bool visible = false)
        {
            Log("Initializing scraper with url:\n" + url + "\n");
            var t = Scraper.CreateScraper(url, null, visible);
            t.Wait();
            s = t.Result;
        }

        public void Shutdown()
        {
            if (s != null)
                s.Shutdown();
        }

        public void SetInputValue(string xPath, string value)
        {
            WaitAppearingElement(xPath);
            Log("Setting input data \"" + value + "\"...");
            var t1 = s.SetInputValue(xPath, value);
            t1.Wait();
            var t2 = s.MakeEvent(xPath, "change");
            t2.Wait();
            var t3 = s.MakeEvent(xPath, "input");
            t3.Wait();
        }

        public void SetInputValueInsideIframe(string iFramePath, string id, string value)
        {
            WaitAppearingElementInIframe(iFramePath, id);
            Log("Setting input data \"" + value + "\"...");
            var t1 = s.SetInputValueInsideIframe(iFramePath, id, value);
            t1.Wait();
            var t2 = s.MakeEventInsideIframe(iFramePath, id, "change");
            t2.Wait();
            var t3 = s.MakeEventInsideIframe(iFramePath, id, "input");
            t3.Wait();
        }

        public void ClickButton(string xPath)
        {
            WaitAppearingElement(xPath);
            Log("Clicking " + ConvertPathToName(xPath) + " button...");
            var t = s.ClickButton(xPath);
            t.Wait();
        }

        public void ClickButtonInsideIframe(string frameXpath, string id)
        {
            Log("Clicking " + ConvertPathToName(id) + " button...");
            var t = s.ClickButtonInsideIframe(frameXpath, id);
            t.Wait();
        }

        public void WaitDisappearingElement(string checkXpath, int seconds = 30)
        {
            WaitAppearingElement(checkXpath, true, seconds);
        }

        public void WaitAppearingElement(string xPath, bool checkDisappearing = false, int seconds = 30)
        {
            for (int i = 0; (checkDisappearing || !Check(xPath)) && (!checkDisappearing || Check(xPath)); i++)
            {
                Thread.Sleep(100);
                if (i == (seconds * 10))
                    throw new ScrapingException(MakeOnErrorActions("Failed waiting appearing " + ConvertPathToName(xPath) + " element"));
            }
            Thread.Sleep(100);
        }

        public void WaitAppearingAnyOfTwoElements(string xPath1, string xPath2, int seconds = 30)
        {
            for (int i = 0; !Check(xPath1) && !Check(xPath2); i++)
            {
                Thread.Sleep(100);
                if (i == (seconds * 10))
                    throw new ScrapingException(MakeOnErrorActions("Failed waiting appearing " +
                        ConvertPathToName(xPath1) + " or " + ConvertPathToName(xPath2) + " element"));
            }
            Thread.Sleep(100);
        }

        public void WaitChangingTextOfElement(string xPath, string text, int seconds = 30)
        {
            for (int i = 0; GetString(xPath) == text; i++)
            {
                Thread.Sleep(100);
                if (i == (seconds * 10))
                    throw new ScrapingException(MakeOnErrorActions("Failed waiting changing text of " + ConvertPathToName(xPath) + " element"));
            }
            Thread.Sleep(100);
        }

        public void WaitAppearingElementInIframe(string iFramePath, string id, bool checkDisappearing = false, int seconds = 30)
        {
            for (int i = 0; (checkDisappearing || !CheckElementInIframe(iFramePath, id)) && (!checkDisappearing || CheckElementInIframe(iFramePath, id)); i++)
            {
                Thread.Sleep(100);
                if (i == (seconds * 10))
                    throw new ScrapingException(MakeOnErrorActions("Failed waiting appearing " + id + " identifier in " + ConvertPathToName(iFramePath) + " iframe"));
            }
            Thread.Sleep(100);
        }

        public bool Check(string xPath)
        {
            var t = s.Check(xPath);
            t.Wait();
            return t.Result;
        }

        public bool Check2(string xPath)
        {
            var t = s.Check2(xPath);
            t.Wait();
            return t.Result;
        }

        public void FreezeHtml(string html = null)
        {
            var t = s.FreezeHtml(html);
            t.Wait();
        }

        public void UnfreezeHtml()
        {
            s.UnfreezeHtml();
        }

        public bool CheckElementInLastIframe(string xPath)
        {
            var t = s.CheckElementInLastIframe(xPath);
            t.Wait();
            return t.Result;
        }

        public bool CheckElementInIframe(string iFramePath, string id)
        {
            var t = s.CheckElementInIframe(iFramePath, id);
            t.Wait();
            return t.Result;
        }

        public string GetIFrameHtml(IFrame frame)
        {
            try
            {
                var t = s.GetIFrameHtml(frame);
                t.Wait();
                return t.Result;
            }
            catch
            {
                return null;
            }
        }

        public void WaitForAppearingElementInLastIframe(string xPath)
        {
            for (int i = 0; !CheckElementInLastIframe(xPath); i++)
            {
                Thread.Sleep(100);
                if (i == 200)
                    throw new ScrapingException(MakeOnErrorActions("Failed waiting appearing " + ConvertPathToName(xPath) + " element"));
            }
            Thread.Sleep(100);
        }

        public void WaitForAppearingElementInIframe(string iFramePath, string id)
        {
            for (int i = 0; !CheckElementInIframe(iFramePath, id); i++)
            {
                Thread.Sleep(100);
                if (i == 200)
                    throw new ScrapingException(MakeOnErrorActions("Failed waiting appearing " + id + " identifier in " + ConvertPathToName(iFramePath) + " iframe"));
            }
            Thread.Sleep(100);
        }

        public string MakeOnErrorActions(string errorText)
        {
            string errorTextForFileName = errorText;
            foreach (char ch in Path.GetInvalidFileNameChars())
                errorTextForFileName = errorTextForFileName.Replace(ch, '.');
            if (errorTextForFileName.EndsWith("."))
                errorTextForFileName = errorTextForFileName.Substring(0, errorTextForFileName.Length - 1);
            string imgName = Path.Combine(LogsDirectoryPath, DateTime.Now.ToString("[yyyy.MM.dd HH.mm.ss] ") +
                errorTextForFileName + ".png");
            Log("Error occured: " + errorText);
            var t = s.GetScreenShot();
            t.Wait();
            Image img = t.Result;
            if (img != null)
            {
                img.Save(imgName);
                Log("See details on screenshot " + imgName);
            }
            return errorText;
        }

        public string GetAttribute(string xPath, string attrName, bool waitAppearing)
        {
            if (waitAppearing)
                WaitAppearingElement(xPath);
            var t = s.GetAttribute(xPath, attrName);
            t.Wait();
            return t.Result;
        }

        public void WaitEnablingButtonAndClick(string xPath)
        {
            WaitAppearingElement(xPath);
            for (int i = 0; ; i++)
            {
                var t2 = s.GetAttribute(xPath, "disabled");
                t2.Wait();
                if (t2.Result == null)
                    break;
                if (i == 200)
                    throw new ScrapingException(MakeOnErrorActions("Error waiting while " + ConvertPathToName(xPath) + " element will be enabled"));
                Thread.Sleep(100);
            }
            Thread.Sleep(100);
            ClickButton(xPath);
        }

        string ConvertPathToName(string xPath)
        {
            if (xPath.IndexOf("'") != -1)
            {
                string[] parts = xPath.Split(new char[] { '\'' });
                return parts[parts.Length - 2];
            }
            return xPath;
        }

        public string GetString(string xPath)
        {
            var t = s.GetString(xPath);
            t.Wait();
            return t.Result;
        }

        public string GetObjectValue(string objectName)
        {
            var t = s.GetObjectValue(objectName);
            t.Wait();
            return (string)t.Result;
        }

        public List<string> GetElementsAttributes(string xPath, string attributeName)
        {
            var t = s.GetElementsAttributes(xPath, attributeName);
            t.Wait();
            return t.Result;
        }

        public List<string> GetStrings(string xPath)
        {
            var t = s.GetStrings(xPath);
            t.Wait();
            return t.Result;
        }

        public string GetHtml()
        {
            var t = s.GetHtml();
            t.Wait();
            return t.Result;
        }

        public void Redirect(string url)
        {
            var t = s.Redirect(url);
            t.Wait();
        }

        public void Refresh()
        {
            var t = s.Refresh();
            t.Wait();
        }

        public void ScrollDownPage()
        {
            var t = s.ScrollDownPage();
            t.Wait();
        }

        public void GoBack()
        {
            var t = s.GoBack();
            t.Wait();
        }

        public void DeleteElement(string fadePath)
        {
            var t = s.DeleteElement(fadePath);
            t.Wait();
        }

        public List<T> Parse<T>(ParseOptions options) where T : new()
        {
            var t = s.Parse<T>(options);
            t.Wait();
            return t.Result;
        }

        public static void Log(string text)
        {
            string LogText = text.Replace("\r\n", "\n").Insert((text.StartsWith("\n") ? 1 : 0), DateTime.Now.ToString("[dd.MM.yyyy HH:mm:ss] "));
            Console.WriteLine(LogText);
            string path = Path.Combine(LogsDirectoryPath, "Log.txt");
            string writingText = (LogText.Replace("\n", "\r\n") + "\r\n");
            while (true)
            {
                try
                {
                    if (!Directory.Exists(LogsDirectoryPath))
                        Directory.CreateDirectory(LogsDirectoryPath);
                    File.AppendAllText(path, writingText);
                    break;
                }
                catch (Exception)
                {
                    Thread.Sleep(10);
                }
            }
        }
    }
}
